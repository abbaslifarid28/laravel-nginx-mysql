# laravel-nginx-mysql



## Getting started
Step 1 — Downloading Laravel and Installing Dependencies

    cd ~
    git clone https://github.com/laravel/laravel.git laravel-app
Move into the laravel-app directory:

    cd ~/laravel-app

Next, use Docker’s composer image to mount the directories that you will need for your Laravel project and avoid the overhead of installing Composer globally:

    docker run --rm -v $(pwd):/app composer install

Using the -v and --rm flags with docker run creates an ephemeral container that will be bind-mounted to your current directory before being removed. This will copy the contents of your ~/laravel-app directory to the container and also ensure that the vendor folder Composer creates inside the container is copied to your current directory.

As a final step, set permissions on the project directory so that it is owned by your non-root user:

    sudo chown -R $USER:$USER ~/laravel-app
